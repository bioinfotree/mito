### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Thu Aug  9 15:44:40 2012 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

ASSEMBLY_FASTA ?=

PRJ ?= test

QUERY_GB ?= 

EVAL ?= 1e-03


assembly.fasta: 
	 ln -sf $(ASSEMBLY_FASTA) $@

features.fasta:
	gb2tab --feature_type MOST --splice --genename < $(QUERY_GB) \
	| select_columns 2 1 4 \
	| cut --delimiter='/' -f 1,2 --output-delimiter=' ' \
	| bsort --key 1,1 \
	| tab2fasta 1 \
	| tr -s [:blank:] '_' > $@


query.fasta: features.fasta
	fastatool sanitize $< $@




# make DB for BLASTN
assembly.flag: assembly.fasta
	mkdir -p $(basename $<); \
	cd $(basename $<); \
	ln -sf ../$< $(basename $<); \
	makeblastdb -in $(basename $<) -dbtype nucl -title $(basename $<); \
	cd ..; \
	touch $@




define search_DB_tab
	blastn -evalue $1 -num_threads $$THREADNUM -outfmt "6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qseq sseq" -max_target_seqs 20 -query $2 -db ./$(basename $3)/$(basename $3) -out $4
endef

define search_DB_pairwise
	blastn -evalue $1 -num_threads $$THREADNUM -outfmt 0 -max_target_seqs 20 -query $2 -db ./$(basename $3)/$(basename $3) -out $4
endef



query.tab: query.fasta assembly.flag
	!threads
	$(call search_DB_tab,$(EVAL),$<,$^2,$@)

query.pairwise: query.fasta assembly.flag
	!threads
	$(call search_DB_pairwise,$(EVAL),$<,$^2,$@)


query_sort.tab: query.tab
	bsort --key=1,1b --key=3,3gr < $< > $@

# you can see if best blast hits share subjects 
bestHitSortedBySubject.tab: query_sort.tab
	select_columns 3 2 1 < $< | uniq -f 2 -c | tr -s ' ' \\t | bsort --key=4,4 > $@

# you can see that mt-tRNA genes are often identified by the same contigs that
# align against polypeptide coding genes. This because they delimit the protein-coding genes
duplicatedSubjects.tab: query_sort.tab
	select_columns 1 2 < $< | bsort --key=2,2 | uniq -D -f 1 > $@




.PHONY: test
test: 1e-50_cdna3.tab cdna3_specific.lst
	@echo

# Standard Phony Targets for Users.

# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += assembly.flag \
	 query_sort.tab \
	 query.pairwise \
	 bestHitSortedBySubject.tab \
	 duplicatedSubjects.tab

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += features.fasta




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += assembly \
	 assembly.fasta \
	 assembly.flag \
	 query.fasta \
	 query.tab \
	 query.pairwise \
	 query_sort.tab \
	 bestHitSortedBySubject.tab \
	 duplicatedSubjects.tab


######################################################################
### phase_1.mk ends here

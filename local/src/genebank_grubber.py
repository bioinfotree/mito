#!/usr/bin/python

# genebank_grubber.py --- 
# 
# Filename: genebank_grubber.py
# Description: 
# Author: Michele Vidotto
# Maintainer: 
# Created: Thu Sep  9 11:02:10 2010 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# TO DO:
# 2 versioni: prima interattiva, seconda permette stabilire parametri ricerca mediante flag per rendere automatico il download
# flag per ottenere lista database disponibili
#


# Commentary: 
# 
# 
# 
#

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:



# Biopython library for searching
# on entrez
from Bio import Entrez
from Bio import SeqIO
import os

import os.path
## Common utility scripts often need to process command line
## arguments. These arguments are stored in the sys modules argv
## attribute as a list. For instance the following output results
## from running python demo.py one two three at the command line
import sys
## The argparse module makes it easy to write user friendly command
## line interfaces.
import argparse





# databases to search
db = u'genome'

# Email need by Entrez for contact you if there are problems with your queries
email = u'michele.vidotto@studenti.unipd.it'

# a string with no internal spaces that identifies the resource that is using
# Entrez links (e.g., tool=flybase). This argument is used to help NCBI provide
# better service to third parties generating Entrez queries from programs
tool = u'flybase'

# file in witch save downloaded data
filename = r""

# Email need by Entrez for contact you if there are problems with your queries
Entrez.email = email

# term to search for. You can use Boolean expressions available for Entrez:
#
# AND: two search terms together instructs Entrez to find all documents that contain BOTH terms
# OR: two search terms together instructs Entrez to find all documents that contain EITHER term.
# NOT: two search terms together instructs Entrez to find all documents that contain search term
# 1 BUT NOT search term 2. 
# e.i.: g1p3 AND (response element OR promoter) you can use partentesis
term = u'acipenser'



def main(term=term):

    # parse command line arguments, return args object
    # containing value for every flag
    args = parse_arguments(term=term, email=email, tool=tool, filename=filename)

    #print 'filename = %s' %(u''.join(args.filename))

    # Email need by Entrez for contact you if there are problems with your queries
    Entrez.email = args.email[0]

    # join all terms used for the research
    term = u' '.join(args.term)

    #term = u'Acipenser[Organism] AND ribosomal[All Fields] NOT mitochondrial[All] NOT mitochondrion[All]'
   
    # Search query in all databases and
    # sort results in decreasing order
    fill_records =  EG_query(term=term)

    print 'Records found in descending order for %s:\n' %(term)

    for item in fill_records:
        print '%s %i' %(item[0], item[1])



    # to make the script automated put en if question to ask the if
    # that will enable the output only if the corresponding parameter
    # was introduced on command line
    
    # to command line
    print '\nChoose a database to search?'
  
    # reads in just a single line from console at a time,
    # and is generally much slower than .readlines()
    # .strip() remove blank chars at the end
    db = sys.stdin.readline().strip()

    # get list of database
    db_list = E_info()
   
    # check if database inserted exist
    if (db not in db_list):
        print u'Database %s not exist!\n' %(db) 
        return 0


    # takes ret_max equal to the number of
    # entries found for the given database
    ret_max = 1000

    for record in fill_records:
        if db in record:
            ret_max = record[1]

    # to command line
    print '\nHow many records you want to download? [default is max = %i]' %(ret_max)
    # control if data were inserted otherwise chouse default
    ret_max_tmp = sys.stdin.readline().strip()
    if ((ret_max_tmp.isspace() == False) and (len(ret_max_tmp) != 0)):
        ret_max = ret_max_tmp
       
    # search terms in the database with a user-determined
    # maximum number of records
    id_list = E_search(db=db, term=term, ret_max=ret_max)



    # to command line
    print '\nDo you want to download the data? [yes/no]\n'    
    # reads in just a single line from console at a time,
    # and is generally much slower than .readlines()
    # .strip() remove blank chars at the end
    dw_query = sys.stdin.readline().strip()
    if (dw_query == u'no'):
        print 'Done!'
        return 0

   
    # EPost uploads a list of UIs for use in subsequent search strategies;
    # see the EPost help page for more information.
    web_env, query_key = E_post(id_list, db)

    # allows to choose in which format can be downloaded nucleotide sequences 
    nucleotide_fetch(db=db, id_list=id_list, web_env=web_env, query_key=query_key, filename=args.filename[0])


    # you can add new functions that allow you to set a specific ret_type and file extension
    # for the type of data that can be downloaded from different databases
    
    return 0





# allows to choose in which format can be downloaded nucleotide sequences
def nucleotide_fetch(db, id_list, web_env, query_key, filename):

    if (db == 'genome'
        or db == 'nuccore'
        or db == 'gene'
        or db == 'genomeprj'
        or db == 'nucest'):

        format_type = u'fasta'

        print '\nIn whitch format? [gb/fasta/both]\n'
        format_type = sys.stdin.readline().strip()
        #print '\nType the name of the destination file with the full path\n'

        
        
        #filename = sys.stdin.readline().strip()

        # check if it is an absolute file name
        #if (os.path.isabs(filename)):

        if (format_type == u'gb') or (format_type == u'both'):
            #Download in genebank format
            ret_type = u'gb'
            # recall E_fetch with different ret_type
            data2file(ret_type=ret_type, filename=filename, db=db, id_list=id_list, web_env=web_env, query_key=query_key)

        if (format_type == u'fasta') or (format_type == u'both'):
            #Download in fasta format
            ret_type = u'fasta'
            # recall E_fetch with different ret_type
            data2file(ret_type=ret_type, filename=filename, db=db, id_list=id_list, web_env=web_env, query_key=query_key)
    
    return 0




# recall E_fetch with different ret_type
def data2file(ret_type, filename, db, id_list, web_env, query_key):

    filename = filename + '.' + ret_type            
    E_fetch(db, id_list, ret_type, web_env, query_key, filename)
    
    return 0





# Search query in all databases and
# sort results in decreasing order
def EG_query(term):

    # EGQuery provides counts for a search term in each of the Entrez
    # databases (i.e. a global query).
    handle = Entrez.egquery(term=term)
    record = Entrez.read(handle)

    # list with only filled records
    fill_records = []

    # print the number of queries found for each database
    for row in record[u'eGQueryResult']:

        # filters only the records for witch are found instances
        # row = dictionary
        if (row[u'Status'] == u'Ok'):
            # create a tuple (DbName, Count)
            T = (row[u'DbName'],int(row[u'Count']))
            # fill list with tuples
            fill_records.append(T)

    # order list of tuples using the 2' value of
    # every tuple as sorting terms
    # for more explanation about sorting list of
    # tuples see: http://wiki.python.org/moin/HowTo/Sorting
    fill_records.sort(key=lambda T: T[1])

    return fill_records 




# get list of database
def E_info():

    # EInfo provides field index term counts, last update,
    # and available links for each of NCBIs databases
    handle = Entrez.einfo()
    result = Entrez.read(handle)

    db_list = result[u'DbList']

    return db_list




# search terms in the database with a user-determined
# maximum number of records
def E_search(db, term, ret_max):
    # TODO: INSERT HISTORY
    

    # search record in the database nucleotide core
    #
    # db=database name
    # usehistory=y
    # WebEnv=WgHmIcDG]
    # query_key=6
    # term=search strategy
    # field=
    # reldate=
    # mindate=
    # maxdate=
    # datetype=edat
    # retstart=x  (x= sequential number of the first record retrieved - default=0
    #             which will retrieve the first record)
    # retmax=y  (y= number of items retrieved)
    # retmode=xml
    # rettype=
    # sort=Use in conjunction with Web Environment to display sorted results in ESummary and EFetch.
    ## PubMed values:

    ##     author
    ##     last+author
    ##     journal
    ##     pub+date

    ## Gene values:

    ##     Weight
    ##     Name
    ##     Chromosome

    handle = Entrez.esearch(db=db, term=term, retmax=ret_max)
    record =  Entrez.read(handle)

    # number of records
    num_results = int(record[u'Count'])
    # list if ids found
    id_list = record[u'IdList']
    # number of items retrieved
    ret_max = int(record[u'RetMax'])
    # translation of the query for Entrez 
    query_translation = record[u'QueryTranslation']

    print u'\nNumber of records found in %s database: %i\n\
          Number of items retrieved: %i\n\
          Query Translation: %s\n' %(db, num_results, ret_max, query_translation)

    return id_list




def E_post(id_list, db):
    
    # EPost uploads a list of UIs for use in subsequent search strategies;
    # see the EPost help page for more information.
    search_results =  Entrez.read(Entrez.epost(db, id=','.join(id_list)))
    webenv = search_results['WebEnv']
    query_key = search_results['QueryKey']

    return (webenv, query_key)




## # ESummary:  Retreives document Summaries from a list of primary IDs or from the user's
## # environment.
## # we can for example find out more about the journal with ID 30367:
## handle = Entrez.esummary(db=u'journals', id=u'30367')
## record = Entrez.read(handle)


def E_fetch(db, id_list, ret_type, web_env, query_key, filename):
    # EFetch:  Retrieves records in the requested format from a list of one or more UIs or from user's
    # environment.
    #
    # db=database name
    # WebEnv=WgHmIcDG]
    # query_key=6
    # 
    # retmode=output format
    # rettype=output types based on database
    #
    # retmode and rettype are different for different databases:
    #
    #  Literature Database (PubMed, Journals, PubMed Central, OMIM)
    #  Sequence and other Molecular Biology Databases (Nucleotide, Protein, Gene, etc.)
    #  Taxonomy

    # fetch records in a bunch of 5 at the same time to reduce the load on NCBI's servers
    #idlist = ','.join(record['IdList'][:5])

    # returns record in GenBank format through xml
    #handle = Entrez.efetch(db=db, id=idlist, retmode='xml')
    #records = Entrez.read(handle)

    print 'Downloading in %s format...\n' % (ret_type)

    count = len(id_list)

    batch_size = 6

    if not os.path.isfile(filename):
    
        file_handle = open(filename, 'w')

        for start in range(0, count, batch_size):
            end = min(count, start + batch_size)
            print 'Going to download record %i to %i' % (start+1, end)

            # returns record in GenBank format
            fetch_handle = Entrez.efetch(db=db, rettype=ret_type, webenv=web_env, query_key=query_key, retmax=batch_size, retstart=start)

            data = fetch_handle.read()
            file_handle.write(data)
            fetch_handle.close()

        file_handle.close()
        print '\nSaved!\n'

    #print 'Parsing records in GeneBank format...\n'
    #record = SeqIO.index(filename, 'genbank')
    

    #print record

    return 0

    
    #records = Entrez.read(handle)
    #text = handle.read()

    #print u'Records in GeneBank format:\n %s' %(text)

    #records = SeqIO.parse(handle, 'gb')
    #for record in records:
        #print '%s, length %i, with %i features' \
              #% (record.name, len(record), len(record.features))

    # save the sequence data to a local file, and then parse it with
    # Bio.SeqIO.
    # filename = "gi_186972394.gbk"

    


## # ELink:  Checks for the existence of an external or Related Articles link from a list of one or
## # more primary IDs;  retrieves IDs and relevancy scores for links to Entrez databases or Related
## # Articles

## # Error for "elink_090910.dtd".
## #
## # This appears to be a temporary glitch with the Entrez database and not biopython.
## pmid = "19304878"
## record = Entrez.read(Entrez.elink(dbfrom='pubmed', id=pmid))








## # ESpell:  Retrieves spelling suggestions, if available.
## # returns the correct spelling of arcipenser 
## handle = Entrez.espell(term='arcipenser')
## record = Entrez.read(handle)

## print record['CorrectedQuery']





# parse command line arguments
def parse_arguments(term, email, tool, filename):

    # create an ArgumentParser object
    parser = argparse.ArgumentParser(description='Import arguments for genebank_grubber')

    # add arguments to be parsed:
    # --probe_path: name of the flag
    # type: type of arguments (string is default)
    # nargs: number of arguments to be expected
    # default: default value of the flag
    # required: indicate that argument is indispensable
    # help: help description for the arguments

    parser.add_argument('--term', nargs='*', required=True, default=term, help='Term to search for. You can use Boolean expressions available for Entrez')

    parser.add_argument('--email', nargs=1, required=True, default=email, help='Email need by Entrez for contact you if there are problems with your queries')

    parser.add_argument('--tool', nargs=1, required=False, default=tool, help='A string with no internal spaces that identifies the resource that is using Entrez links (e.g., tool=flybase)')

    parser.add_argument('--filename', nargs=1, required=True, default=filename, help='File in witch save downloaded data')

    # call function for parsing arguments
    args = parser.parse_args()
    
    return args





# execute the main function
if __name__ == "__main__":
    main()


# 
# genebank_grubber.py ends here


